\documentclass{tutosjmagnin}
%\usepackage{fullpage}

\title{\href{http://www.jmagnin.fr/?page_id=168}{Importer des données au format texte brut avec Calc}}
\author{Jerry Magnin}
\licence{CC-By-SA}

\begin{document}
\maketitle
\tableofcontents
\listoffigures

\part{Pourquoi des données au format texte brut?}
\section{L’origine des données}
\startpar
Généralement, les capteurs de mesures divers enregistrent leurs données sous forme de fichiers texte brut. Certains sites permettent également l’export de leurs données au format CSV.
Pourquoi ces formats ? Ils présentent plusieurs avantages:
\stoppar
\begin{itemize}
	\item à volume de données égal, un fichier texte brut est moins lourd qu’un fichier de tableur (à titre d’exemple, le fichier qui m’a servi de base pour les captures d’écran de ce tutoriel ---~4 lignes pour 3 colonnes~--- pèse 75 octets en texte brut, contre \SI{16.2}{\kilo o} en fichier calc);
	\item ils peuvent être lus sur presque n’importe quel système d’exploitation, généralement sans avoir besoin d’installer un logiciel particulier (le bloc-notes suffit à les ouvrir);
	\item ils peuvent facilement être scriptés pour en intégrer le contenu dans des bases de données;
	\item ils peuvent être importés directement dans des outils de traitement statistiques comme \texttt{R}.
\end{itemize}

\section{Comment reconnaître un fichier texte brut?}
L’extension des fichiers est généralement un bon indicateur de son type. Un fichier portant l’extension \code{.txt}, \code{.csv} ou \code{.dat} est généralement un fichier texte brut.
Pour en avoir le cœur net, il est possible, sous \gnux, de faire appel à la commande \code{file}, qui, utilisée sans paramètre, vous retourne le type de fichier. Si le type de retour est \code{ASCII text}, vous avez un fichier texte brut (voir figure \ref{fig::commandeFile}).

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{00-File}
	\caption{La commande \code{file} et son retour sur un fichier texte brut}\label{fig::commandeFile}
\end{figure}

\part{Importer directement un fichier texte brut}
\section{L’import}\label{sec::importFichier}
Pour être utilisés dans un tableur (ici Calc), ces fichiers doivent être importés en définissant un certain nombre de paramètres.
Tout d’abord, après avoir enregistré \lien{http://www.jmagnin.fr/wp-content/uploads/2015/02/Tutoriel.csv}{le fichier du tutoriel} sur votre disque dur, ouvrez-le avec un clic droit~> \code{Ouvrir (avec)}~> \code{LibreOffice Calc}.
Une fenêtre similaire à celle de la figure \ref{fig::fenetreImport} devrait apparaître.

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{01-import}
	\caption{Fenêtre d’import de texte brut}\label{fig::fenetreImport}
\end{figure}

Définissez le séparateur (c’est-à-dire le caractère délimitant les différentes colonnes) dans \quot{Options de séparateur}. Les données affichées dans \quot{Champs} s’actualisent en fonction du ou des séparateurs que vous définissez, pour vous donner un aperçu du résultat après import.

Dans la section \quot{Champs}, justement, vous pouvez définir un certain nombre de formats pour vos colonnes.
Pour ce faire, sélectionnez le champ désiré en cliquant sur son en-tête, et modifiez son attribut (par défaut \quot{Standard}) avec la liste déroulante \quot{Type de colonne}.
Pour sélectionner plusieurs champs contigus, sélectionnez le premier puis, en maintenant la touche \code{maj} enfoncée, sélectionnez le dernier.

La première colonne du fichier de données du tutoriel contient des dates au format \quot{année - mois - jour}. Sélectionnez-la, et choisissez le format \quot{Date (AMJ)} (figure \ref{fig::definitionDate}).
Les deux colonnes suivantes utilisent des données chiffrées avec un point en guise de séparateur décimal. Ce format, c’est celui d’un nombre dans les pays anglo-saxons.
Si nous importons ces colonnes sans rien faire, Calc considérera qu’il s’agit de texte.
Il sera alors impossible d’utiliser des formules, de faire des graphiques\dots
Pour que ces colonnes soient reconnues comme des nombres, il faut indiquer que leur format est \quot{Anglais US} (figure \ref{fig::definitionPointDecimal}).

\begin{figure}[hbtp]
	\centering
	\begin{subfigure}[b]{.75\textwidth}
		\includegraphics[width=\textwidth]{02-import-date}
		\caption{Définition d’un type de date}\label{fig::definitionDate}
	\end{subfigure}
	\\
	\begin{subfigure}[b]{.75\textwidth}
		\includegraphics[width=\textwidth]{03-import-pointdecimal}
		\caption{Type \quot{Anglais US}}
		\label{fig::definitionPointDecimal}
	\end{subfigure}
	\caption{Définition des types de colonnes}\label{fig::definitionTypeColonnes}
\end{figure}

Une fois ces opérations préliminaires effectuées, cliquez sur \code{OK} pour vous retrouver dans votre tableur. Les données ont été importées. Notez que Calc a converti le point décimal en virgule, et qu’il affiche la date au format JJ/MM/AA (figure \ref{fig::donneesImportees}).

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=8cm]{04-donnees_importees}
	\caption{Les données après import}\label{fig::donneesImportees}
\end{figure}


\section{Enregistrement du fichier}
Vous allez sans doute vouloir effectuer des calculs sur vos données importées, ou réaliser des graphiques\dots
Ces objets ne peuvent être sauvegardés dans un fichier texte brut. Il faut donc impérativement enregistrer vos données en \code{.ods} en passant par \code{Fichier}~>\code{Enregistrer sous}, en imposant l’extension \code{.ods}. Si vous tentez d’enregistrer en \code{.csv}, LibreOffice vous rappellera que les objets ne peuvent être sauvés dans ce format, et vous demandera confirmation pour savoir si oui ou non vous voulez enregistrer votre feuille au format texte brut.
Si vous répondez par la négative, la boîte de dialogue \quot{Enregistre sous} s’affichera.
Vous pouvez changer le nom de fichier, mais ce n’est pas obligatoire.
Il n’est pas nécessaire de spécifier l’extension \code{.ods}, LibreOffice s’en chargera.

\part{Mettre en forme des données collées dans un tableur}
Quelquefois, les données texte brut que vous aurez proviendront d’un copier-coller.
Plutôt que de les enregistrer dans un fichier texte brut pour ensuite les importer comme vu ci-dessus, il est possible de les intégrer directement dans Calc.

Parfois, lors du coller, LibreOffice vous affichera automatiquement la boîte de dialogue de la figure \ref{fig::fenetreImport}. Vous n’avez plus qu’à suivre les indications de la section \ref{sec::importFichier}.

Mais, quelquefois, les données se retrouveront collées dans une cellule, sans mise en forme.
Sélectionnez alors cette cellule, puis ouvrez le menu \code{Données}~>~\code{Texte en colonnes}
Cette opération vous ouvrira la fenêtre de mise en forme de texte en colonnes, quasi-identique à la fenêtre d’import (au titre près). Suivez alors les indications de la section \ref{sec::importFichier}.

\end{document}